package Zadanie2;

import java.io.*;
import java.util.Scanner;

public class MainZadanie2 {
    public MainZadanie2() throws IOException {
    }

    public static void main(String[] args) {


        Scanner scanner = new Scanner(System.in);
        try (PrintWriter writer = new PrintWriter(new FileWriter("plike.txt", true))) {
            while (scanner.hasNextLine()) {
                System.out.println("Imię i nazwisko");
                String imie = scanner.nextLine();
                if (imie.equals("quit")) {
                    break;
                }

                System.out.println("Wiek: ");
                String wiek = scanner.nextLine();
                System.out.println("Płeć [kobieta/mężczyzna: ");
                String płeć = scanner.nextLine();
                int wiekInt = Integer.parseInt(wiek);

                writer.println(imie);
                writer.println(wiek);
                writer.println(płeć);
                if (płeć.equals("kobieta") && wiekInt > 18 && wiekInt < 25) {
                    System.out.println("dodatkowe pytanie: ");
                    String dodatkowePytanie = scanner.nextLine();
                    writer.println(dodatkowePytanie);
                    if (płeć.equals("mężczyzna") && wiekInt > 25 && wiekInt < 30) {
                        System.out.println("dodatkowe pytanie: ");
                        String dodatkowePyt = scanner.nextLine();
                        writer.println(dodatkowePyt);

                    }

                }}


            } catch(IOException e){
                e.printStackTrace();
            }
        }
    }

/*
Rozwijamy dotychczasową aplikację. Teraz - oczekujemy od użytkownika konkretnych danych,
które będą zapisywane do pliku. Aplikacja będzie służyć jako formularz który zbiera dane
(jak z ankiety). Zakładamy ankietę użytkownika która pyta użytkownika o następujące dane:

Podaj imie i nazwisko:
Podaj wiek:
Podaj płeć:

Kolejne pytania zadajemy dopiero wtedy, gdy użytkownik jest:
a) kobietą w wieku między 18-25
b) mężczyzną w wieku między 25-30

(powiedzmy że chodzi o jednakową dojrzałość osób biorących udział w ankiecie)
Wymyśl 3 pytania i zadaj je ankietowanemu.
Wszystkie pytania i odpowiedzi ankiety zapisz do pliku.
Otwieraj plik wyłącznie do dopisywania danych (jeśli istnieje).

Rozszerzenie:
Stwórz klasę modelu (Form) która przechowuje odpowiedzi na wszystkie pytania.
Dodaj metodę toString, która będzie używana do wypisania obiektu do pliku.

 */