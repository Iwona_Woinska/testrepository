package Zadanie1;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

public class MainZadanie1 {
    public static void main(String[] args) {
        File plik = new File("plik.txt");
        Scanner scaner = new Scanner(System.in);

        try (PrintWriter writer = new PrintWriter("plik.txt")) {

            String linia = null;
            while (scaner.hasNextLine()) {
                linia = scaner.nextLine();
                System.out.println("Linia: " + linia);
                writer.println(linia);
                if (linia.equals("quit")) {
                    break;
                }
                writer.flush();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
//        do {
//
//                writer.println();
//
//
//        } while (quit);
    }
}

/*
Należy napisać aplikację (wykorzystując załączony kod) która w pętli while czyta ze Scannera
wejście użytkownika z konsoli, a następnie linia po linii wypisuje tekst do pliku.
Aplikacja ma się zamykać po wpisaniu przez użytkownika komendy "quit".
 */